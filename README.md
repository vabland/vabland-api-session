# Vabland API Session

Responsible to manage user session, like:
- Login
- Logout
- Current status of a token session

-----

### Requirements

- Java 8
- Docker
- Lombok Plugin
- Enable Annotation Processing in Intellij Settings

-----

### Shortcuts

Recreate and start a database
```bash
./vabland database
```

Recreate and start a database and start local application without docker. Access: https://localhost/api/session/example/1
```bash
./vabland local
```

Recreate and start a database and rebuild the application and rebuilt docker image and than start the application. Access: https://localhost:1002/api/session/example/1
```bash
./vabland docker
```

Recreate and start a database and runs all tests
```bash
./vabland test
```

Stop and destroy all containers
```bash
./vabland stop
```

-----

### Database
To start local database run:
```bash
docker-compose up -d database
```

To erase the database container run:
```bash
docker-compose rm database
```

Local database information:
```
user: root
password: password
database: session
schema: session
ip: localhost
port: 2002
```

-----

### Building and Running: Gradle

Database container should be running.

To build application jar:
```bash
./gradlew clean build
```

To run all tests
```bash
./gradlew test
```

To run locally, execute: 
```bash
./gradlew bootRun
```

To verify if the application is running, access:
```
https://localhost/session/api/example/1
```

-----

### Building and Running: Docker

To build an image, first execute the following command to generate the jar:
```bash
./gradlew clean build
```

Run the docker command to generate the image based and running
```bash
docker-compose up
```

To verify if the application is running, access:
```
https://localhost:1002/session/api/example/1
```

To force a new image generation, run:
```bash
docker-compose up --build
```

To erase the all running containers:
```bash
docker-compose rm
```

-----

### Docker Machine Configuration 
First, initialize docker virtual machine using docker-machine.
```bash
docker-machine start
eval $(docker-machine env)
```

To stop docker virtual machine:
```bash
docker-machine stop
```

To see the IP associated with the virtual machine:
```bash
docker-machine ip
```

When using docker-machine, by default, all containers could be accessed through the IP **192.168.99.100**. 
To change this behaviour and to make it accessible through **localhost** execute the following steps:

- Install Virtual Box Manager (https://www.virtualbox.org/wiki/Downloads)
- Open Virtual Box Manager and go to Settings of your docker vitual machine (usually the name is **default**)
- Go to Network section and click and **Advanced** in **Adapter 1** and **Port Forwarding**
- In the window, add two new rules:
    - session api (protocol: **TCP**, Host IP: **127.0.0.1**, Host Port: **1002**, Guest IP: **empty**, Guest Port: **1002**)
    - session database (protocol: **TCP**, Host IP: **127.0.0.1**, Host Port: **2002**, Guest IP: **empty**, Guest Port: **2002**)

-----

### Security Decisions

The application runs below HTTPS even in local environment.
So any requests to the application should display a _**insecure service message**_.
This happens because the application is using a self-signed SSL certificate for local development.
The local configuration of this SSL certificate is:
```
type: PKCS12
file name: classpath:local-keystore.p12
alias: local
password: password
```
It was used java keytool to generate this local SSL Certificate.

To avoid Google Chrome notification about untrusted localhost certificates, access:
```
chrome://flags/#allow-insecure-localhost
```

To generate a new Self-signed SSL certificate run:
```bash
keytool -genkeypair -alias <your_alias> -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore <keystore_name>.p12 -validity 3650
```

To validates the Self-signed SSL certificate:
```bash
keytool -list -v -storetype pkcs12 -keystore <keystore_name>.p12
```

Link for SSL Certificate generation can be found here: https://www.thomasvitale.com/https-spring-boot-ssl-certificate/


[https-spring-boot-ssl-certificate]: https://www.thomasvitale.com/https-spring-boot-ssl-certificate/

[test]: https://www.thomasvitale.com/https-spring-boot-ssl-certificate/
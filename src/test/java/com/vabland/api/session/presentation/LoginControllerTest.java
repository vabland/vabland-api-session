package com.vabland.api.session.presentation;

import com.vabland.api.session.domain.exception.LoginFailedException;
import com.vabland.api.session.domain.service.LoginService;
import com.vabland.api.session.presentation.request.LoginRequest;
import com.vabland.api.session.presentation.response.ApiErrorResponse;
import com.vabland.api.session.presentation.response.ApiErrorsResponse;
import com.vabland.api.session.presentation.response.SessionResponse;
import com.vabland.api.session.utils.fixture.Fixture;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

@ExtendWith(MockitoExtension.class)
@DisplayName("LoginController Class")
class LoginControllerTest {

    private static final String INVALID_EMAIL_OR_PASSWORD_CODE = "InvalidEmailOrPassword";
    private static final String INVALID_EMAIL_OR_PASSWORD_MESSAGE = "Email and password combination does not match any user";

    @Mock
    private LoginService loginService;

    @InjectMocks
    private LoginController loginController;

    @Nested
    @DisplayName("login Method")
    class LoginMethod {

        @Test
        @DisplayName("should returns a OK response with body filled with same response returned from LoginService.login")
        void returnsSessionResponseWhenUsernameAndPasswordMatches() {
            LoginRequest loginRequest = Fixture.loginRequest().build();
            SessionResponse sessionResponse = Fixture.sessionResponse().build();
            when(loginService.login(loginRequest)).thenReturn(sessionResponse);

            ResponseEntity<SessionResponse> response = loginController.login(loginRequest);

            assertAll("should return an instance of ResponseEntity with expected content",
                    () -> assertThat(response.getStatusCode()).isEqualTo(OK),
                    () -> assertThat(response.getBody()).isEqualTo(sessionResponse)
            );
        }

        @Test
        @DisplayName("should not handle a LoginFailedException when it was throw by LoginService.login")
        void shouldNotTreatLoginErrorExceptionWhenServiceThrowIt() {
            assertThrows(LoginFailedException.class, () -> {
                LoginRequest loginRequest = Fixture.loginRequest().build();
                when(loginService.login(loginRequest)).thenThrow(new LoginFailedException());

                loginController.login(loginRequest);
            });
        }

    }

    @Nested
    @DisplayName("loginFailedHandler Method")
    class LoginFailedHandlerMethod {

        @Test
        @DisplayName("should returns a BAD REQUEST response with body filled with APIErrorsResponse instance built using static text")
        void returnBadRequestWithBodyWhenHandlingLoginErrorException() {
            ApiErrorResponse apiErrorResponse = new ApiErrorResponse(INVALID_EMAIL_OR_PASSWORD_CODE, INVALID_EMAIL_OR_PASSWORD_MESSAGE);
            List<ApiErrorResponse> errorsResponses = singletonList(apiErrorResponse);
            ApiErrorsResponse apiErrorsResponse = new ApiErrorsResponse(BAD_REQUEST.name(),
                    BAD_REQUEST.value(),
                    LocalDateTime.now(),
                    errorsResponses);

            ResponseEntity<ApiErrorsResponse> response = loginController.loginFailedHandler(new LoginFailedException());

            assertAll("should return an instance of ResponseEntity with expected content",
                    () -> assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST),
                    () -> assertThat(response.getBody().getCode()).isEqualTo(apiErrorsResponse.getCode()),
                    () -> assertThat(response.getBody().getStatus()).isEqualTo(apiErrorsResponse.getStatus()),
                    () -> assertThat(response.getBody().getErrors().get(0).getCode()).isEqualTo(apiErrorResponse.getCode()),
                    () -> assertThat(response.getBody().getErrors().get(0).getMessage()).isEqualTo(apiErrorResponse.getMessage())
            );
        }

    }
}
package com.vabland.api.session.presentation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.OK;

import com.vabland.api.session.application.utils.Token;
import com.vabland.api.session.domain.service.SessionService;
import com.vabland.api.session.presentation.response.SessionResponse;
import com.vabland.api.session.utils.fixture.Fixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
@DisplayName("SessionController Class")
class SessionControllerTest {

    @Mock
    private SessionService sessionService;

    @InjectMocks
    private SessionController sessionController;

    @Nested
    @DisplayName("renew Method")
    class RenewMethod {

        private String token;

        @BeforeEach
        void setUp() {
            token = Token.create();
        }

        @Test
        @DisplayName("should call SessionService Renew method using the given token")
        void shouldCallSessionServiceRenewUsingTheGivenToken() {
            sessionController.renew(token);

            verify(sessionService).renew(token);
        }

        @Test
        @DisplayName("should returns OK and SessionResponse object when SessionService finds it")
        void shouldReturnsOKAndSessionResponseWhenSessionServiceFindsIt() {
            SessionResponse sessionResponse = Fixture.sessionResponse()
                    .token(token)
                    .build();
            when(sessionService.renew(token)).thenReturn(Optional.of(sessionResponse));

            ResponseEntity<SessionResponse> response = sessionController.renew(token);

            assertThat(response.getStatusCode()).isEqualTo(OK);
            assertThat(response.getBody()).isEqualTo(sessionResponse);
        }
    }
}
package com.vabland.api.session.presentation;

import com.vabland.api.session.application.utils.Token;
import com.vabland.api.session.domain.exception.LogoutFailedException;
import com.vabland.api.session.domain.service.LogoutService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

@ExtendWith(MockitoExtension.class)
@DisplayName("LogoutController Class")
class LogoutControllerTest {

    @Nested
    @DisplayName("logout Method")
    class LogoutMethod {

        @Mock
        private LogoutService logoutService;

        @InjectMocks
        private LogoutController logoutController;
        private String token;

        @BeforeEach
        void setUp() {
            token = Token.create();
        }

        @Test
        @DisplayName("should call LogoutService logout method passing the Token received from request")
        void shouldCallLogoutServiceLogoutMethodPassingTheTokenAsParameter() {
            logoutController.logout(token);

            verify(logoutService).logout(token);
        }

        @Test
        @DisplayName("should return OK when LogoutService doesn't throw any exception")
        void shouldReturnOKWhenLogoutServiceDoesNotThrowAnyException() {
            ResponseEntity<Void> response = logoutController.logout(token);

            assertThat(response.getStatusCode()).isEqualTo(OK);
        }

        @Test
        @DisplayName("should return NOT FOUND when LogoutService throws LogoutFailedException")
        void shouldReturnNOTFOUNDWhenLogoutServiceThrowsLogoutFailedException() {
            doThrow(new LogoutFailedException()).when(logoutService).logout(token);

            ResponseEntity<Void> response = logoutController.logout(token);

            assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
        }

    }
}
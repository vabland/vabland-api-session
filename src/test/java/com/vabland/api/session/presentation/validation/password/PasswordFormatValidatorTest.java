package com.vabland.api.session.presentation.validation.password;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintValidatorContext;

import static com.vabland.api.session.utils.random.RandomAttributeUtils.randomPassword;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@DisplayName("PasswordFormatValidator Class")
class PasswordFormatValidatorTest {

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    private PasswordFormatValidator passwordFormatValidator;

    @BeforeEach
    void setUp() {
        passwordFormatValidator = new PasswordFormatValidator();
    }

    @Nested
    @DisplayName("isValid Method")
    class IsValidMethod {

        @Test
        @DisplayName("should returns true when password have all specifications")
        void isValidPassword() {
            String password = randomPassword();

            boolean valid = passwordFormatValidator.isValid(password, constraintValidatorContext);

            assertThat(valid).isTrue();
        }

        @Test
        @DisplayName("should returns false when password doesn't contain at least one digit")
        void isInvalidPasswordBecauseDoesNotContainsAtLeastOneDigit() {
            String password = randomAlphabetic(1).toLowerCase() +
                    randomAlphabetic(1).toUpperCase() +
                    randomAlphabetic(8);

            boolean valid = passwordFormatValidator.isValid(password, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

        @Test
        @DisplayName("should returns false when password doesn't contain at least one lower case letter")
        void isInvalidPasswordBecauseDoesNotContainsAtLeastOneLowerCase() {
            String password = randomNumeric(1) +
                    randomAlphabetic(8).toUpperCase();

            boolean valid = passwordFormatValidator.isValid(password, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

        @Test
        @DisplayName("should returns false when password doesn't contain at least one upper case letter")
        void isInvalidPasswordBecauseDoesNotContainsAtLeastOneUpperCase() {
            String password = randomNumeric(1) +
                    randomAlphabetic(8).toLowerCase();

            boolean valid = passwordFormatValidator.isValid(password, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

        @Test
        @DisplayName("should returns false when password doesn't contain at least 8 characters")
        void isInvalidPasswordBecauseDoesNotContainsAtLeastEightCharacters() {
            String password = randomNumeric(1) +
                    randomAlphabetic(1).toLowerCase() +
                    randomAlphabetic(1).toUpperCase() +
                    randomAlphabetic(4);

            boolean valid = passwordFormatValidator.isValid(password, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

        @Test
        @DisplayName("should returns false when password is null")
        void isInvalidPasswordBecauseItIsNull() {
            boolean valid = passwordFormatValidator.isValid(null, constraintValidatorContext);

            assertThat(valid).isFalse();
        }

    }
}
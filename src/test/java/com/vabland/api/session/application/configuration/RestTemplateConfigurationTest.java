package com.vabland.api.session.application.configuration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@DisplayName("RestTemplateConfiguration Class")
class RestTemplateConfigurationTest {

    @InjectMocks
    private RestTemplateConfiguration restTemplateConfiguration;

    @Nested
    @DisplayName("buildRestTemplate Method")
    class BuildRestTemplateMethod {

        @Test
        @DisplayName("Should returns an instance of RestTemplate create by RestTemplateBuilder")
        void shouldReturnsAnInstanceOfRestTemplate() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
            RestTemplate restTemplate = restTemplateConfiguration.buildRestTemplate();

            assertThat(restTemplate).isInstanceOf(RestTemplate.class);
        }
    }
}

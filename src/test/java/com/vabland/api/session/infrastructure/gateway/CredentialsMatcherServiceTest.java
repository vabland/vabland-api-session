package com.vabland.api.session.infrastructure.gateway;

import com.vabland.api.session.domain.service.CredentialsMatcherService;
import com.vabland.api.session.infrastructure.gateway.response.EncodedPasswordResponse;
import com.vabland.api.session.utils.fixture.Fixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;

import static com.vabland.api.session.utils.random.RandomAttributeUtils.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayName("CredentialsMatcherService Class")
class CredentialsMatcherServiceTest {

    @Mock
    private APIGateway apiGateway;

    @Mock
    private PasswordEncoder passwordEncoder;

    private CredentialsMatcherService credentialsMatcherService;
    private String apiUserHost;

    @BeforeEach
    void setUp() {
        apiUserHost = "http://localhost/api/user";

        credentialsMatcherService = new CredentialsMatcherService(apiGateway, passwordEncoder, apiUserHost);
    }

    @Nested
    @DisplayName("matches Method")
    class matchesMethod {

        @Mock
        private APIGateway.APIGatewayRequestBuilder apiGatewayBuilder;
        private String email;
        private String password;
        private String encodedPassword;

        @BeforeEach
        void setUp() {
            email = randomEmail();
            password = randomPassword();
            encodedPassword = randomEncodedPassword();
            EncodedPasswordResponse encodedPasswordResponse = Fixture.encodedPasswordResponse()
                    .encodedPassword(encodedPassword)
                    .build();
            ResponseEntity<EncodedPasswordResponse> response = ResponseEntity.ok(encodedPasswordResponse);
            when(apiGateway.get(apiUserHost)).thenReturn(apiGatewayBuilder);
            when(apiGatewayBuilder.paths("users", email)).thenReturn(apiGatewayBuilder);
            when(apiGatewayBuilder.execute(EncodedPasswordResponse.class)).thenReturn(response);
        }

        @Test
        @DisplayName("should calls APIGateway and the get chain methods using the provided email")
        void shouldCallsAPIUserUsingAPIGateway() {
            credentialsMatcherService.matches(email, password);

            verify(apiGateway).get(apiUserHost);
            verify(apiGatewayBuilder).paths("users", email);
            verify(apiGatewayBuilder).execute(EncodedPasswordResponse.class);
        }

        @Test
        @DisplayName("should return true when user is found and password matches")
        void shouldReturnTrueWhenUserIsFoundAndPasswordMatches() {
            when(passwordEncoder.matches(password, encodedPassword)).thenReturn(true);

            boolean matches = credentialsMatcherService.matches(email, password);

            assertThat(matches).isTrue();
        }

        @Test
        @DisplayName("should return false when user is found but password don't match")
        void shouldReturnFalseWhenUserIsFoundButPasswordDoesNotMatch() {
            when(passwordEncoder.matches(password, encodedPassword)).thenReturn(false);

            boolean matches = credentialsMatcherService.matches(email, password);

            assertThat(matches).isFalse();
        }

        @Test
        @DisplayName("should return false when user is not found")
        void shouldReturnAnInstanceOfPasswordEvenWhenUserIsNotFound() {
            ResponseEntity<EncodedPasswordResponse> notFoundResponse = ResponseEntity.notFound().build();
            when(apiGatewayBuilder.execute(EncodedPasswordResponse.class)).thenReturn(notFoundResponse);

            boolean matches = credentialsMatcherService.matches(email, password);

            assertThat(matches).isFalse();
        }
    }
}
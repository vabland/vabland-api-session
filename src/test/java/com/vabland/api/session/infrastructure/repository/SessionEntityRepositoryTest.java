package com.vabland.api.session.infrastructure.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.repository.CrudRepository;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@DisplayName("SessionEntityRepository Class")
class SessionEntityRepositoryTest {

    @Mock
    private SessionRepository sessionRepository;

    @Test
    @DisplayName("should be an instance of CrudRepository")
    void shouldBeAnInstanceOfCrudRepository() {
        assertThat(sessionRepository).isInstanceOf(CrudRepository.class);
    }
}
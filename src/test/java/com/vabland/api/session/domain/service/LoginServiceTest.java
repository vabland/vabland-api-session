package com.vabland.api.session.domain.service;

import com.vabland.api.session.application.utils.Token;
import com.vabland.api.session.domain.entity.SessionEntity;
import com.vabland.api.session.domain.exception.LoginFailedException;
import com.vabland.api.session.infrastructure.repository.SessionRepository;
import com.vabland.api.session.presentation.request.LoginRequest;
import com.vabland.api.session.presentation.response.SessionResponse;
import com.vabland.api.session.utils.fixture.Fixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.vabland.api.session.utils.random.RandomAttributeUtils.randomEmail;
import static com.vabland.api.session.utils.random.RandomAttributeUtils.randomPassword;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("LoginService Class")
class LoginServiceTest {

    @Mock
    private CredentialsMatcherService credentialsMatcherService;

    @Mock
    private SessionRepository sessionRepository;

    @InjectMocks
    private LoginService loginService;

    @Nested
    @DisplayName("login Method")
    class LoginMethod {

        private String token;
        private String email;
        private String password;
        private LoginRequest loginRequest;
        private SessionEntity sessionEntity;

        @BeforeEach
        void setUp() {
            token = Token.create();
            email = randomEmail();
            password = randomPassword();

            loginRequest = Fixture.loginRequest()
                    .email(email)
                    .password(password)
                    .build();

            sessionEntity = Fixture.sessionEntity()
                    .token(token)
                    .email(loginRequest.getEmail())
                    .build();
        }

        @Test
        @DisplayName("should call CredentialsMatcherService to validates the email combined with password")
        void shouldCallAPIUserClientUsingTheProvidedEmailToRetrieveUserToBeCompared() {
            when(credentialsMatcherService.matches(email, password)).thenReturn(true);
            when(sessionRepository.save(any())).thenReturn(sessionEntity);

            loginService.login(loginRequest);

            verify(credentialsMatcherService).matches(email, password);
        }

        @Test
        @DisplayName("should throw LoginFailedException when credentials doesn't match")
        void shouldThrowLoginFailedExceptionWhenPasswordsDoesNotMatch() {
            when(credentialsMatcherService.matches(email, password)).thenReturn(false);

            assertThrows(LoginFailedException.class, () -> loginService.login(loginRequest));
        }

        @Test
        @DisplayName("should call SessionRepository to save the new sessionEntity when credentials are valid")
        void shouldSaveSessionUsingSessionRepository() {
            when(credentialsMatcherService.matches(email, password)).thenReturn(true);
            ArgumentCaptor<SessionEntity> sessionCaptor = ArgumentCaptor.forClass(SessionEntity.class);
            when(sessionRepository.save(sessionCaptor.capture())).thenReturn(sessionEntity);

            loginService.login(loginRequest);

            assertAll("should save sessionEntity using email from request",
                    () -> assertThat(sessionCaptor.getValue().getEmail()).isEqualTo(email)
            );
        }

        @Test
        @DisplayName("should return a SessionResponse when credentials are valid and the sessionEntity was persisted")
        void shouldReturnSessionResponseWhenCredentialsAndSaveProcessGoesWell() {
            when(credentialsMatcherService.matches(email, password)).thenReturn(true);
            ArgumentCaptor<SessionEntity> sessionCaptor = ArgumentCaptor.forClass(SessionEntity.class);
            when(sessionRepository.save(sessionCaptor.capture())).thenReturn(sessionEntity);

            SessionResponse sessionResponse = loginService.login(loginRequest);

            assertAll("should return SessionResponse with data from SessionEntity Entity",
                    () -> assertThat(sessionResponse.getToken()).isEqualTo(sessionEntity.getToken()),
                    () -> assertThat(sessionResponse.getEmail()).isEqualTo(sessionEntity.getEmail()),
                    () -> assertThat(sessionResponse.getExpiresAt()).isEqualTo(sessionEntity.getExpiresAt())
            );
        }
    }

}
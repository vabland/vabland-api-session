package com.vabland.api.session.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.vabland.api.session.application.utils.Token;
import com.vabland.api.session.domain.entity.SessionEntity;
import com.vabland.api.session.infrastructure.repository.SessionRepository;
import com.vabland.api.session.presentation.response.SessionResponse;
import com.vabland.api.session.utils.fixture.Fixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
@DisplayName("SessionService Class")
class SessionServiceTest {

    @Mock
    private SessionRepository sessionRepository;

    @InjectMocks
    private SessionService sessionService;

    @Nested
    @DisplayName("renew Method")
    class RenewMethod {

        private String token;

        @BeforeEach
        void setUp() {
            token = Token.create();
        }

        @Test
        @DisplayName("should tries to find a SessionEntity in the repository using the given Token")
        void shouldTriesToFindAnSessionUsingTheGivenToken() {
            when(sessionRepository.findById(token)).thenReturn(Optional.empty());

            sessionService.renew(token);

            verify(sessionRepository).findById(token);
        }

        @Test
        @DisplayName("should returns an Optional empty when repository doesn't contains the given token")
        void shouldReturnsAnOptionalEmptyWhenRepositoryDoesNotContainsTheGivenToken() {
            when(sessionRepository.findById(token)).thenReturn(Optional.empty());

            Optional<SessionResponse> sessionResponseOptional = sessionService.renew(token);

            assertThat(sessionResponseOptional.isPresent()).isFalse();
        }

        @Test
        @DisplayName("should returns a SessionResponse when a SessionEntity is found in the repository using the given Token")
        void shouldReturnsASessionEntityWhenFoundInTheRepositoryUsingTheGivenToken() {
            SessionEntity sessionEntity = Fixture.sessionEntity()
                    .token(token)
                    .build();
            when(sessionRepository.findById(token)).thenReturn(Optional.of(sessionEntity));
            when(sessionRepository.save(any())).thenReturn(sessionEntity);

            Optional<SessionResponse> optionalSessionResponse = sessionService.renew(token);

            assertThat(optionalSessionResponse.isPresent()).isTrue();
            assertThat(optionalSessionResponse.get().getToken()).isEqualTo(token);
            assertThat(optionalSessionResponse.get().getEmail()).isNotNull();
            assertThat(optionalSessionResponse.get().getExpiresAt()).isNotNull();
        }

        @Test
        @DisplayName("should returns a SessionResponse with a renewed expiredAt field when it is found in the repository for the given Token")
        void shouldReturnsASessionResponseWithARenewedExpiredAtFieldWhenItIsFoundInTheRepositoryForTheGiventoken() {
            LocalDateTime expiresAtBefore = LocalDateTime.now().plusMonths(5);
            SessionEntity sessionEntity = Fixture.sessionEntity()
                    .token(token)
                    .expiresAt(expiresAtBefore)
                    .build();
            when(sessionRepository.findById(token)).thenReturn(Optional.of(sessionEntity));

            ArgumentCaptor<SessionEntity> sessionEntityCaptor = ArgumentCaptor.forClass(SessionEntity.class);
            when(sessionRepository.save(sessionEntity)).thenReturn(sessionEntity);

            sessionService.renew(token);

            verify(sessionRepository).save(sessionEntityCaptor.capture());
            sessionEntity = sessionEntityCaptor.getValue();
            assertThat(sessionEntity.getExpiresAt().isAfter(expiresAtBefore)).isTrue();
        }
    }
}
package com.vabland.api.session.domain.entity;

import static com.vabland.api.session.utils.random.RandomAttributeUtils.randomEmail;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

@DisplayName("SessionEntity Class")
class SessionEntityTest {

    @Nested
    @DisplayName("Constructor")
    class Constructor {

        @Test
        @DisplayName("should create an instance with random token")
        void shouldBeCreatedWithRandomToken() {
            String email = randomEmail();

            SessionEntity sessionEntity = new SessionEntity(email);

            assertThat(sessionEntity.getToken()).isNotNull();
            assertThat(sessionEntity.getToken()).hasSize(100);
        }

    }

    @Nested
    @DisplayName("prePersist Method")
    class PrePersistMethod {

        private SessionEntity sessionEntity;

        @BeforeEach
        void setUp() {
            sessionEntity = new SessionEntity(randomEmail());
        }

        @Test
        @DisplayName("should expires in 6 months")
        void shouldBeCreatedWithRandomToken() {
            LocalDateTime before = LocalDateTime.now().plusMonths(6).minusMinutes(1);
            sessionEntity.prePersist();
            LocalDateTime after = LocalDateTime.now().plusMonths(6).plusMinutes(1);

            assertThat(sessionEntity.getExpiresAt().isAfter(before)).isTrue();
            assertThat(sessionEntity.getExpiresAt().isBefore(after)).isTrue();
        }

        @Test
        @DisplayName("should mark sessionEntity created in the current time")
        void shouldMarkSessionCreatedAtWithCurrentTime() {
            LocalDateTime before = LocalDateTime.now().minusMinutes(1);
            sessionEntity.prePersist();
            LocalDateTime after = LocalDateTime.now().plusMinutes(1);

            assertThat(sessionEntity.getCreatedAt().isAfter(before)).isTrue();
            assertThat(sessionEntity.getCreatedAt().isBefore(after)).isTrue();
        }

        @Test
        @DisplayName("should createdAt be the same of updatedAt")
        void shouldCreatedAtBeTheSameOfUpdatedAt() {
            sessionEntity.prePersist();

            assertThat(sessionEntity.getUpdatedAt()).isEqualTo(sessionEntity.getCreatedAt());
        }

    }

    @Nested
    @DisplayName("preUpdate Method")
    class PreUpdateMethod {

        private SessionEntity sessionEntity;

        @BeforeEach
        void setUp() {
            sessionEntity = new SessionEntity(randomEmail());
        }

        @Test
        @DisplayName("should update updatedAt with current time")
        void shouldUpdateUpdatedAtWithCurrentTime() {
            LocalDateTime before = LocalDateTime.now().minusMinutes(1);
            sessionEntity.preUpdate();
            LocalDateTime after = LocalDateTime.now().plusMinutes(1);

            assertThat(sessionEntity.getUpdatedAt().isAfter(before)).isTrue();
            assertThat(sessionEntity.getUpdatedAt().isBefore(after)).isTrue();
        }

    }

    @Nested
    @DisplayName("renew Method")
    class RenewMethod {

        private SessionEntity sessionEntity;

        @BeforeEach
        void setUp() {
            sessionEntity = new SessionEntity(randomEmail());
        }

        @Test
        @DisplayName("should renew expires for the next 6 months")
        void shouldBeCreatedWithRandomToken() {
            LocalDateTime before = LocalDateTime.now().plusMonths(6).minusMinutes(1);
            sessionEntity.renew();
            LocalDateTime after = LocalDateTime.now().plusMonths(6).plusMinutes(1);

            assertThat(sessionEntity.getExpiresAt().isAfter(before)).isTrue();
            assertThat(sessionEntity.getExpiresAt().isBefore(after)).isTrue();
        }


    }

}
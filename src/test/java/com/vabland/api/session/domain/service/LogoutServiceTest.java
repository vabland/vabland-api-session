package com.vabland.api.session.domain.service;

import com.vabland.api.session.application.utils.Token;
import com.vabland.api.session.domain.entity.SessionEntity;
import com.vabland.api.session.domain.exception.LogoutFailedException;
import com.vabland.api.session.infrastructure.repository.SessionRepository;
import com.vabland.api.session.utils.fixture.Fixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayName("LogoutService Class")
class LogoutServiceTest {

    @Mock
    private SessionRepository sessionRepository;

    @InjectMocks
    private LogoutService logoutService;

    @Nested
    @DisplayName("logout Method")
    class LogoutMethod {

        private String token;

        @BeforeEach
        void setUp() {
            token = Token.create();
        }

        @Test
        @DisplayName("should delete SessionEntity when it is found in repository")
        void shouldDeleteSessionEntityWhenFoundInRepository() {
            SessionEntity sessionEntity = Fixture.sessionEntity().build();
            when(sessionRepository.findById(token)).thenReturn(Optional.of(sessionEntity));

            logoutService.logout(token);

            verify(sessionRepository).delete(sessionEntity);
        }

        @Test
        @DisplayName("should throw LogoutFailedException when session is not found by the given token")
        void shouldThrowLogoutFailedExceptionWhenSessionIfNotFoundByTheGivenToken() {
            when(sessionRepository.findById(token)).thenReturn(Optional.empty());

            assertThrows(LogoutFailedException.class, () -> logoutService.logout(token));
        }
    }

}
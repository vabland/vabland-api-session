package com.vabland.api.session.utils.fixture;

import com.vabland.api.session.presentation.request.LoginRequest;

import static com.vabland.api.session.utils.random.RandomAttributeUtils.randomEmail;
import static com.vabland.api.session.utils.random.RandomAttributeUtils.randomPassword;

public class LoginRequestBuilder {

    private String email = randomEmail();
    private String password = randomPassword();

    LoginRequestBuilder() {}

    public LoginRequestBuilder email(String email) {
        this.email = email;
        return this;
    }

    public LoginRequestBuilder password(String password) {
        this.password = password;
        return this;
    }

    public LoginRequest build() {
        return new LoginRequest(email, password);
    }
}

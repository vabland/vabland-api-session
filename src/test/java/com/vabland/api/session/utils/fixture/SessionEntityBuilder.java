package com.vabland.api.session.utils.fixture;

import com.vabland.api.session.application.utils.Token;
import com.vabland.api.session.domain.entity.SessionEntity;

import java.time.LocalDateTime;

import static com.vabland.api.session.utils.random.RandomAttributeUtils.randomEmail;

public class SessionEntityBuilder {

    private String token = Token.create();
    private String email = randomEmail();
    private LocalDateTime expiresAt = LocalDateTime.now().plusMonths(6);
    private LocalDateTime createdAt = LocalDateTime.now();
    private LocalDateTime updatedAt = LocalDateTime.now();

    SessionEntityBuilder() {}

    public SessionEntityBuilder token(String token) {
        this.token = token;
        return this;
    }

    public SessionEntityBuilder email(String email) {
        this.email = email;
        return this;
    }

    public SessionEntityBuilder expiresAt(LocalDateTime expiresAt) {
        this.expiresAt = expiresAt;
        return this;
    }

    public SessionEntity build() {
        return new SessionEntity(token, email, expiresAt, createdAt, updatedAt);
    }
}

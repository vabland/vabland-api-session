package com.vabland.api.session.utils.fixture;

import com.vabland.api.session.application.utils.Token;
import com.vabland.api.session.presentation.response.SessionResponse;

import java.time.LocalDateTime;

import static com.vabland.api.session.utils.random.RandomAttributeUtils.randomEmail;

public class SessionResponseBuilder {

    private String token = Token.create();
    private String email = randomEmail();
    private LocalDateTime expiresAt = LocalDateTime.now();

    SessionResponseBuilder() {}

    public SessionResponseBuilder token(String token) {
        this.token = token;
        return this;
    }

    public SessionResponse build() {
        return new SessionResponse(token, email, expiresAt);
    }

}

package com.vabland.api.session.utils.fixture;

public class Fixture {

    public static EncodedPasswordBuilder encodedPasswordResponse() {
        return new EncodedPasswordBuilder();
    }

    public static LoginRequestBuilder loginRequest() {
        return new LoginRequestBuilder();
    }

    public static SessionEntityBuilder sessionEntity() {
        return new SessionEntityBuilder();
    }

    public static SessionResponseBuilder sessionResponse() {
        return new SessionResponseBuilder();
    }

}

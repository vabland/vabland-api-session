package com.vabland.api.session.utils.fixture;

import com.vabland.api.session.infrastructure.gateway.response.EncodedPasswordResponse;

import static com.vabland.api.session.utils.random.RandomAttributeUtils.randomEncodedPassword;

public class EncodedPasswordBuilder {

    private String encodedPassword = randomEncodedPassword();

    EncodedPasswordBuilder() {}

    public EncodedPasswordBuilder encodedPassword(String encodedPassword) {
        this.encodedPassword = encodedPassword;
        return this;
    }

    public EncodedPasswordResponse build() {
        return new EncodedPasswordResponse(encodedPassword);
    }

}

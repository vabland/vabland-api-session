package com.vabland.api.session.presentation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@AllArgsConstructor
public class ApiErrorsResponse {

    private String status;
    private Integer code;
    private LocalDateTime timestamp;
    private List<ApiErrorResponse> errors;

}

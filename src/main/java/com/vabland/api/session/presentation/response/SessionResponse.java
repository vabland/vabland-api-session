package com.vabland.api.session.presentation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class SessionResponse {

    private String token;
    private String email;
    private LocalDateTime expiresAt;

}

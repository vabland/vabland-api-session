package com.vabland.api.session.presentation;

import com.vabland.api.session.domain.exception.LogoutFailedException;
import com.vabland.api.session.domain.service.LogoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
class LogoutController {

    @Autowired
    private LogoutService logoutService;

    @DeleteMapping(value = "/logout/{token}")
    ResponseEntity<Void> logout(@PathVariable("token") String token) {
        try {
            logoutService.logout(token);
        } catch (LogoutFailedException exception) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

}

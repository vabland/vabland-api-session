package com.vabland.api.session.presentation;

import com.vabland.api.session.domain.exception.LoginFailedException;
import com.vabland.api.session.domain.service.LoginService;
import com.vabland.api.session.presentation.request.LoginRequest;
import com.vabland.api.session.presentation.response.ApiErrorResponse;
import com.vabland.api.session.presentation.response.ApiErrorsResponse;
import com.vabland.api.session.presentation.response.SessionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDateTime;

import static java.util.Collections.singletonList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
class LoginController {

    private static final String INVALID_EMAIL_OR_PASSWORD_CODE = "InvalidEmailOrPassword";
    private static final String INVALID_EMAIL_OR_PASSWORD_MESSAGE = "Email and password combination does not match any user";

    @Autowired
    private LoginService loginService;

    @PostMapping(value = "/login")
    ResponseEntity<SessionResponse> login(@Valid @RequestBody LoginRequest request) {
        SessionResponse sessionResponse = loginService.login(request);
        return ResponseEntity.ok(sessionResponse);
    }

    @ExceptionHandler(LoginFailedException.class)
    ResponseEntity<ApiErrorsResponse> loginFailedHandler(LoginFailedException ex) {
        ApiErrorResponse error = new ApiErrorResponse(INVALID_EMAIL_OR_PASSWORD_CODE, INVALID_EMAIL_OR_PASSWORD_MESSAGE);
        ApiErrorsResponse apiErrorsResponse = new ApiErrorsResponse(
                BAD_REQUEST.name(),
                BAD_REQUEST.value(),
                LocalDateTime.now(),
                singletonList(error)
        );
        return ResponseEntity.status(BAD_REQUEST).body(apiErrorsResponse);
    }

}

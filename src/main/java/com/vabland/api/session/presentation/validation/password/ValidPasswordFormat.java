package com.vabland.api.session.presentation.validation.password;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = PasswordFormatValidator.class)
@Documented
public @interface ValidPasswordFormat {

    String message() default "invalid password format because it not contains at least one digit or one lower case letter or one uppercase letter";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}

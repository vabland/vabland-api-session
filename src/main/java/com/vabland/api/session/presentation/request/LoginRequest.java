package com.vabland.api.session.presentation.request;

import com.vabland.api.session.presentation.validation.email.ValidEmailFormat;
import com.vabland.api.session.presentation.validation.password.ValidPasswordFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@AllArgsConstructor
public class LoginRequest {

    @NotNull
    @NotEmpty
    @ValidEmailFormat
    @Size(min = 3, max=180)
    private String email;

    @NotNull
    @NotEmpty
    @Size(min = 8, max=40)
    @ValidPasswordFormat
    private String password;

}

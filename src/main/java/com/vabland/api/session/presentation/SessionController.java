package com.vabland.api.session.presentation;

import com.vabland.api.session.domain.service.SessionService;
import com.vabland.api.session.presentation.response.SessionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class SessionController {

    @Autowired
    private SessionService sessionService;

    @GetMapping("/renew/{token}")
    ResponseEntity<SessionResponse> renew(@PathVariable("token") String token) {
        Optional<SessionResponse> optionalSessionResponse = sessionService.renew(token);

        if (optionalSessionResponse.isPresent()) {
            return ResponseEntity.ok(optionalSessionResponse.get());
        }

        return ResponseEntity.notFound().build();
    }
}

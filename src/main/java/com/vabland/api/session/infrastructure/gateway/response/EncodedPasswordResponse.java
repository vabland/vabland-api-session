package com.vabland.api.session.infrastructure.gateway.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EncodedPasswordResponse {

    private String encodedPassword;

}

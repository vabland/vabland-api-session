package com.vabland.api.session.domain.entity;

import com.vabland.api.session.application.utils.Token;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "session")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SessionEntity {

    @Id
    private String token;
    private String email;
    private LocalDateTime expiresAt;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public SessionEntity(String email) {
        this.token = Token.create();
        this.email = email;
    }

    public void renew() {
        expiresAt = LocalDateTime.now().plusMonths(6);
    }

    @PrePersist
    public void prePersist() {
        expiresAt = LocalDateTime.now().plusMonths(6);
        createdAt = LocalDateTime.now();
        updatedAt = createdAt;
    }

    @PreUpdate
    public void preUpdate() {
        updatedAt = LocalDateTime.now();
    }


}

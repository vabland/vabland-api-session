package com.vabland.api.session.domain.service;

import com.vabland.api.session.domain.entity.SessionEntity;
import com.vabland.api.session.domain.exception.LoginFailedException;
import com.vabland.api.session.infrastructure.repository.SessionRepository;
import com.vabland.api.session.presentation.request.LoginRequest;
import com.vabland.api.session.presentation.response.SessionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    @Autowired
    private CredentialsMatcherService credentialsMatcherService;

    @Autowired
    private SessionRepository sessionRepository;

    public SessionResponse login(LoginRequest loginRequest) {
        String email = loginRequest.getEmail();
        String password = loginRequest.getPassword();

        credentialsValidator(email, password);
        SessionEntity sessionEntity = createSession(email);

        return new SessionResponse(sessionEntity.getToken(), sessionEntity.getEmail(), sessionEntity.getExpiresAt());
    }

    private SessionEntity createSession(String email) {
        SessionEntity sessionEntity = new SessionEntity(email);
        sessionEntity = sessionRepository.save(sessionEntity);
        return sessionEntity;
    }

    private void credentialsValidator(String email, String password) {
        boolean matches = credentialsMatcherService.matches(email, password);

        if (!matches) {
            throw new LoginFailedException();
        }
    }
}

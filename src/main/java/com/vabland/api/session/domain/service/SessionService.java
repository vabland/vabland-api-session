package com.vabland.api.session.domain.service;

import com.vabland.api.session.domain.entity.SessionEntity;
import com.vabland.api.session.infrastructure.repository.SessionRepository;
import com.vabland.api.session.presentation.response.SessionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SessionService {

    @Autowired
    private SessionRepository sessionRepository;

    public Optional<SessionResponse> renew(String token) {
        Optional<SessionEntity> optionalSessionEntity = sessionRepository.findById(token);

        if (optionalSessionEntity.isPresent()) {
            SessionEntity sessionEntity = renewSession(optionalSessionEntity.get());

            SessionResponse sessionResponse = new SessionResponse(sessionEntity.getToken(), sessionEntity.getEmail(), sessionEntity.getExpiresAt());
            return Optional.of(sessionResponse);
        }

        return Optional.empty();
    }

    private SessionEntity renewSession(SessionEntity sessionEntity) {
        sessionEntity.renew();
        sessionEntity = sessionRepository.save(sessionEntity);
        return sessionEntity;
    }

}

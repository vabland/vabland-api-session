package com.vabland.api.session.domain.exception;

public class APIGatewayConnectionException extends RuntimeException {

    public APIGatewayConnectionException(String url) {
        super("The service: " + url + " didn't respond");
    }

}

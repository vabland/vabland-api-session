package com.vabland.api.session.domain.service;

import com.vabland.api.session.infrastructure.gateway.APIGateway;
import com.vabland.api.session.infrastructure.gateway.response.EncodedPasswordResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static org.springframework.http.HttpStatus.OK;

@Service
public class CredentialsMatcherService {

    private static final String USERS_PATH = "users";

    private APIGateway apiGateway;
    private PasswordEncoder passwordEncoder;

    private String apiUserURL;

    public CredentialsMatcherService(@Autowired APIGateway apiGateway,
                                     @Autowired PasswordEncoder passwordEncoder,
                                     @Value("${api.user.url}") String apiUserURL) {
        this.apiGateway = apiGateway;
        this.passwordEncoder = passwordEncoder;
        this.apiUserURL = apiUserURL;
    }

    public boolean matches(String email, String password) {
        ResponseEntity<EncodedPasswordResponse> response = getUser(email);

        if (response.getStatusCode().equals(OK) && response.getBody() != null) {
            return passwordEncoder.matches(password, response.getBody().getEncodedPassword());
        }

        return false;
    }

    private ResponseEntity<EncodedPasswordResponse> getUser(String email) {
        return apiGateway.get(apiUserURL)
                    .paths(USERS_PATH, email)
                    .execute(EncodedPasswordResponse.class);
    }
}

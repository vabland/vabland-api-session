package com.vabland.api.session.domain.service;

import com.vabland.api.session.domain.entity.SessionEntity;
import com.vabland.api.session.domain.exception.LogoutFailedException;
import com.vabland.api.session.infrastructure.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LogoutService {

    @Autowired
    private SessionRepository sessionRepository;

    public void logout(String token) {
        Optional<SessionEntity> optionalSession = sessionRepository.findById(token);
        if (!optionalSession.isPresent()) {
            throw new LogoutFailedException();
        }

        sessionRepository.delete(optionalSession.get());
    }
}
